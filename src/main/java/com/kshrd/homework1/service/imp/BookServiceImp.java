package com.kshrd.homework1.service.imp;

import com.kshrd.homework1.repository.BookRepository;
import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.service.BookService;
import com.kshrd.homework1.service.Util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepository bookRepository;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

      @Override
    public List<BookDto> select(Pagination pagination) {
        return bookRepository.select(pagination);
    }
    @Override
    public BookDto insert(BookDto books) {
        boolean isInserted = bookRepository.insert(books);
        if (isInserted){
            return books;
        }
        else
            return null;

    }

    @Override
    public boolean delete(int id) {
        return bookRepository.delete(id);
    }

    @Override
    public BookDto findId(int id) {
        return bookRepository.findId(id);
    }

    @Override
    public BookDto update(BookDto bookDto) {
        boolean isUpdated =bookRepository.update(bookDto);
        if(isUpdated)
            return bookDto;
        else
            return null;
    }

    @Override
    public int countAllBooks() {
        return bookRepository.countAllBooks();
    }
}

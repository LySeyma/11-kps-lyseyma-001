package com.kshrd.homework1.service.imp;

import com.kshrd.homework1.repository.CategoryRepository;
import com.kshrd.homework1.repository.dto.CategoriesDto;
import com.kshrd.homework1.service.CategoryService;
import com.kshrd.homework1.service.Util.Pagination;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    public CategoryRepository categoryRepository;
    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoriesDto insert(CategoriesDto categoriesDto) {
        boolean isInserted = categoryRepository.insert(categoriesDto);
        if (isInserted){
            return categoriesDto;
        }
        else {
            return null;
        }
    }

    @Override
    public List<CategoriesDto> select(Pagination pagination) {
        return categoryRepository.select(pagination);
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }
    @Override
    public CategoriesDto findId(int id) {
        return categoryRepository.findId(id);
    }

    @Override
    public CategoriesDto update(CategoriesDto categoriesDto) {
        boolean isUpdated =categoryRepository.update(categoriesDto);
        if(isUpdated)
            return categoriesDto;
        else
            return null;

    }
    @Override
    public int countAllBooks() {
        return categoryRepository.countAllBooks();
    }
}

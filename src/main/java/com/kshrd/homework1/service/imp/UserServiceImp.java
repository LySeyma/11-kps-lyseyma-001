package com.kshrd.homework1.service.imp;

import com.kshrd.homework1.repository.UserRepository;
import com.kshrd.homework1.repository.dto.RoleDto;
import com.kshrd.homework1.repository.dto.UserDto;
import com.kshrd.homework1.rest.response.Messages;
import com.kshrd.homework1.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;

public class UserServiceImp implements UserService {
    private UserRepository userRepository;
    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }
    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
    {
        UserDto userDto = userRepository.findUserByUserName(userName);
        if (userDto == null)
            throw new UsernameNotFoundException("User not found");
        return new User(userDto.getUsername(), userDto.getPassword(), new ArrayList<>());
    }

//    @Override
//    public UserDto findUserByUserName(String userName) {
//        return null;
//    }
    //    private UserRepository userRepository;
//
//    @Autowired
//    public UserServiceImp(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return userRepository.selectUserByUsername(username);
//    }
//
//    @Override
//    public UserDto insert(UserDto userDto) {
//        try {
//            userRepository.insert(userDto);
//            int id = userRepository.selectIdByUserId(userDto.getUser_id());
//            userDto.setId(id);
//            for (RoleDto role : userDto.getRoles()) {
//                userRepository.createUserRoles(userDto, role);
//            }
//            return userDto;
//        } catch (Exception e) {
//            System.out.println("Error");
//            throw new ResponseStatusException(
//                    HttpStatus.BAD_REQUEST,
//                    Messages.Error.INSERT_FAILURE.getMessage()
//            );
//        }
//    }
}

package com.kshrd.homework1.service;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.service.Util.Pagination;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto bookDto);
    List<BookDto> select(Pagination pagination);
    boolean delete(int id);
    BookDto findId(int id);
    BookDto update(BookDto bookDto);

    int countAllBooks();
}

package com.kshrd.homework1.service;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.repository.dto.CategoriesDto;
import com.kshrd.homework1.service.Util.Pagination;

import java.util.List;

public interface CategoryService {
    CategoriesDto insert(CategoriesDto categoriesDto);
    List<CategoriesDto> select(Pagination pagination);
    boolean delete(int id);
    CategoriesDto findId(int id);
    CategoriesDto update(CategoriesDto categoriesDto);
    int countAllBooks();
}

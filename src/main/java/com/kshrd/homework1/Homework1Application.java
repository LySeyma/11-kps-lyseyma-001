package com.kshrd.homework1;

import com.kshrd.homework1.service.BookService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
//@EnableConfigurationProperties(StorageProperties.class)
public class Homework1Application {

    public static void main(String[] args) {
        SpringApplication.run(Homework1Application.class, args);
    }
//    @Bean
//    CommandLineRunner init(BookService bookService) {
//        return (args) -> {
//            bookService.deleteAll();
//            bookService.init();
//        };
//    }

}

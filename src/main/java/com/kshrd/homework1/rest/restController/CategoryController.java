package com.kshrd.homework1.rest.restController;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.repository.dto.CategoriesDto;
import com.kshrd.homework1.rest.request.BookRequestModel;
import com.kshrd.homework1.rest.request.CategoryRequestModel;
import com.kshrd.homework1.rest.response.BaseApiResponse;
import com.kshrd.homework1.service.CategoryService;
import com.kshrd.homework1.service.Util.Pagination;
import com.kshrd.homework1.service.imp.CategoryServiceImp;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v2")
public class CategoryController{

    private CategoryServiceImp categoryService;
    private CategoryService categoryService1;
    @Autowired
    public void setCategoryService(CategoryServiceImp categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping("/category")
    public ResponseEntity<BaseApiResponse<List<CategoryRequestModel>>> select(@RequestParam(value = "page", required = true,defaultValue = "1")int page,
                                                                          @RequestParam(value = "limit",required = true,defaultValue = "4")int limit){
        ModelMapper mapper = new ModelMapper();
        Pagination pagination = new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();

        pagination.setTotalCount(categoryService.countAllBooks());
        pagination.setTotalPages(pagination.getTotalPages());
        BaseApiResponse<List<CategoryRequestModel>> response = new BaseApiResponse<>();
        List<CategoriesDto> categoriesDtos = categoryService.select(pagination);

        List<CategoryRequestModel>categoryRequest =new ArrayList<>();
        for(CategoriesDto categoriesDto:categoriesDtos){
            categoryRequest.add(mapper.map(categoriesDto,CategoryRequestModel.class));
        }

        response.setMessage("You have found Books successfully");
        response.setPagination(pagination);
        response.setData(categoryRequest);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/category")
    public ResponseEntity <BaseApiResponse<CategoryRequestModel>>insert(@RequestBody CategoryRequestModel category){
        BaseApiResponse<CategoryRequestModel>response= new BaseApiResponse<>();
        ModelMapper mapper= new ModelMapper();
        CategoriesDto categoriesDto =mapper.map(category,CategoriesDto.class);
        CategoriesDto result =categoryService.insert(categoriesDto);
        CategoryRequestModel result2= mapper.map(result,CategoryRequestModel.class);
        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }
    @GetMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> findId(@PathVariable int id,@RequestBody CategoriesDto categories) {
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<CategoryRequestModel> response= new BaseApiResponse<>();
        CategoriesDto categoriesDto= mapper.map(id,CategoriesDto.class);
        CategoriesDto result = categoryService.findId(id);
        if (result==null){
            response.setMessage("You have not found a category successfully");
        }
        else {
            CategoryRequestModel category = (mapper.map(result, CategoryRequestModel.class));
            response.setMessage("You have found a book successfully");
            response.setData(category);
        }
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> delete(@PathVariable int id,@RequestBody CategoriesDto categories){
        ModelMapper mapper= new ModelMapper();
        BaseApiResponse<CategoryRequestModel> response=new BaseApiResponse<>();
        CategoriesDto categoriesDto =mapper.map(id,CategoriesDto.class);
        CategoriesDto result =categoryService.findId(id);
        if (result==null){
            response.setMessage("ID Not Found Delete Unsuccessfully");
        }
        else {
            categoryService.delete(id);
            response.setMessage("You have delete book successfully");
        }
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> update(@PathVariable int id,@RequestBody CategoryRequestModel requestModel){
        ModelMapper modelMapper=new ModelMapper();
        BaseApiResponse<CategoryRequestModel> response=new BaseApiResponse <>();
        CategoriesDto dto=modelMapper.map(requestModel,CategoriesDto.class);
        dto.setId(id);
        System.out.println(dto.toString());
        CategoryRequestModel responseModel=modelMapper.map(categoryService.update(dto),CategoryRequestModel.class);
        response.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        response.setStatus(HttpStatus.OK);
        response.setData(responseModel);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}

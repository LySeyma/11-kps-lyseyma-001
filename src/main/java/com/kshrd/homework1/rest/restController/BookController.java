package com.kshrd.homework1.rest.restController;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.rest.request.BookRequestModel;
import com.kshrd.homework1.rest.response.BaseApiResponse;
import com.kshrd.homework1.service.BookService;
import com.kshrd.homework1.service.Util.Pagination;
import com.kshrd.homework1.service.imp.BookServiceImp;
//import com.sun.org.apache.xalan.internal.utils.XMLSecurityManager;
import io.swagger.models.Model;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v2/books")
public class BookController {
    private BookServiceImp bookServiceImp;

    private BookService bookService;

    @Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @Autowired
    public void setBookServiceImp(BookServiceImp bookServiceImp) {
        this.bookServiceImp = bookServiceImp;
    }

//    @Autowired
//    public void FileUploadController(BookService bookService) {
//        this.bookService = bookService;
//    }

//    @GetMapping("/upload")
//    public String listUploadedFiles(Model model) throws IOException {
//
//        model.addAttribute("files", bookService.loadAll().map(
//                path -> MvcUriComponentsBuilder.fromMethodName(BookController.class,
//                        "serveFile", path.getFileName().toString()).build().toUri().toString())
//                .collect(Collectors.toList()));
//
//        return "uploadForm";
//    }
    @PostMapping("")
    public ResponseEntity <BaseApiResponse<BookRequestModel>>insert(@RequestBody BookRequestModel book){
        BaseApiResponse<BookRequestModel>response= new BaseApiResponse<>();
        ModelMapper mapper= new ModelMapper();
        BookDto bookDto =mapper.map(book,BookDto.class);
        BookDto result =bookServiceImp.insert(bookDto);
        BookRequestModel result2= mapper.map(result,BookRequestModel.class);
        response.setMessage("You have added book successfully");
        response.setData(result2);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));

        return ResponseEntity.ok(response);
    }

    @GetMapping("")
    public ResponseEntity<BaseApiResponse<List<BookRequestModel>>> select(@RequestParam(value = "page", required = true,defaultValue = "1")int page,
                                                                      @RequestParam(value = "limit",required = true,defaultValue = "4")int limit){
        ModelMapper mapper = new ModelMapper();
        Pagination pagination = new Pagination(page,limit);
        pagination.setPage(page);
        pagination.setLimit(limit);
        pagination.nextPage();
        pagination.previousPage();

        pagination.setTotalCount(bookService.countAllBooks());
        pagination.setTotalPages(pagination.getTotalPages());
        BaseApiResponse<List<BookRequestModel>> response = new BaseApiResponse<>();
        List<BookDto> bookDtos = bookService.select(pagination);

        List<BookRequestModel>bookResponses =new ArrayList<>();
        for(BookDto bookDto:bookDtos){
            bookResponses.add(mapper.map(bookDto,BookRequestModel.class));
        }

        response.setMessage("You have found Books successfully");
        response.setPagination(pagination);
        response.setData(bookResponses);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> delete(@PathVariable int id,@RequestBody BookDto book){
        ModelMapper mapper= new ModelMapper();
        BaseApiResponse<BookRequestModel> response=new BaseApiResponse<>();
        BookDto bookDto =mapper.map(id,BookDto.class);
        BookDto result =bookServiceImp.findId(id);
        if (result==null){
            response.setMessage("ID Not Found Delete Unsuccessfully");
        }
        else {
            bookServiceImp.delete(id);
            response.setMessage("You have delete book successfully");
        }
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> update(@PathVariable int id,@RequestBody BookRequestModel requestModel){
        ModelMapper modelMapper=new ModelMapper();
        BaseApiResponse<BookRequestModel> response=new BaseApiResponse <>();
        BookDto dto=modelMapper.map(requestModel,BookDto.class);
        dto.setId(id);
        System.out.println(dto.toString());
        BookRequestModel responseModel=modelMapper.map(bookService.update(dto),BookRequestModel.class);
        response.setMessage("YOU HAVE UPDATED SUCCESSFULLY!");
        response.setStatus(HttpStatus.OK);
        response.setData(responseModel);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
    @GetMapping("/{id}")
    public ResponseEntity<BaseApiResponse<BookRequestModel>> findId(@PathVariable int id,@RequestBody BookDto books) {
        ModelMapper mapper = new ModelMapper();
        BaseApiResponse<BookRequestModel> response= new BaseApiResponse<>();
        BookDto bookDto= mapper.map(id,BookDto.class);
        BookDto result = bookServiceImp.findId(id);
        if (result==null){
            response.setMessage("You have not found a book successfully");
        }
        else {
            BookRequestModel book = (mapper.map(result, BookRequestModel.class));
            response.setMessage("You have found a book successfully");
            response.setData(book);
        }
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        return ResponseEntity.ok(response);
    }
}

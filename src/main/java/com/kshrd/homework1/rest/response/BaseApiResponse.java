package com.kshrd.homework1.rest.response;

import com.kshrd.homework1.service.Util.Pagination;
import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class BaseApiResponse<T> {
        private boolean success;
    private Pagination pagination;
        private String message;
        private T data;
        private HttpStatus status;
        private Timestamp time;

        public BaseApiResponse(){}

    public BaseApiResponse(boolean success,Pagination pagination ,String message, T data, HttpStatus status, Timestamp time) {
        this.success = success;
        this.pagination=pagination;
        this.message = message;
        this.data = data;
        this.status = status;
        this.time = time;

    }

    public boolean isSuccess() {
        return success;
    }


    public void setSuccess(boolean success) {
        this.success = success;
    }
    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }



    @Override
    public String toString() {
        return "BaseApiResponse{" +
                "pagination='" + pagination +
                ",success=" + success +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", status=" + status +
                ", time=" + time +

                '}';
    }

}

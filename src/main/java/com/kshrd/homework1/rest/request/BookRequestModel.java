package com.kshrd.homework1.rest.request;

public class BookRequestModel {
    private int id;
    private String title;
    private String author;
    private String thumbnail;
    private String description;
    public  BookRequestModel(){};

    public BookRequestModel(int id,String title, String author, String thumbnail, String description) {
        this.id=id;
        this.title = title;
        this.author = author;
        this.thumbnail = thumbnail;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "BookRequestModel{" +
                "id='" + id + '\'' +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}

package com.kshrd.homework1.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }

    public String selectByFilter(String articleId, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");

            // Check condition
            if (articleId != null || !articleId.equals(""))
                WHERE("id = #{id}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }
}

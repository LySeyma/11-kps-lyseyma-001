package com.kshrd.homework1.repository.provider;

import org.apache.ibatis.jdbc.SQL;

public class BookProvider {

    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }

    public String selectByFilter(String articleId, String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");

            // Check condition
            if (articleId != null || !articleId.equals(""))
                WHERE("id = #{id}");
            else if (title != null || !title.equals(""))
                WHERE("title = #{title}");
            else
                WHERE();

        }}.toString();
    }
    public String selectCategoryTitleById(){
        return new SQL(){{
            SELECT("title");
            FROM("categories");
            WHERE("id=#{id}");

        }}.toString();
    }
    public String selectCatById(){
        return new  SQL(){{
            SELECT("*");
            FROM("categories");
            WHERE("id=#{id}");
        }}.toString();
    }
}

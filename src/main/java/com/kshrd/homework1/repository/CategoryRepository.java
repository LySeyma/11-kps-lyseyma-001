package com.kshrd.homework1.repository;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.repository.dto.CategoriesDto;
import com.kshrd.homework1.repository.provider.BookProvider;
import com.kshrd.homework1.service.Util.Pagination;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepository {
    @Select("SELECT COUNT(id) FROM tb_books")
    int countAllBooks();
    @Insert("INSERT INTO tb_categories (title) VALUES (#{title})")
    boolean insert (CategoriesDto categoriesDto);
    @Select("SELECT * FROM tb_categories  LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
//    @SelectProvider(value = CategoryRepository.class, method = "select")
    List<CategoriesDto> select(@Param("pagination") Pagination pagination);
    @Delete("DELETE FROM tb_categories WHERE id=#{id}")
    boolean delete(int id);
    @Select("SELECT * FROM tb_categories WHERE id=#{id}")
    CategoriesDto findId(int id);
    @Update("UPDATE tb_categories SET title=#{title}" +
            " WHERE id=#{id}")
    boolean update(CategoriesDto categoriesDto);
}

package com.kshrd.homework1.repository.dto;

public class CategoriesDto {
    private int id;
    private String title;
    public CategoriesDto(){}

    public CategoriesDto(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoriesDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }
}

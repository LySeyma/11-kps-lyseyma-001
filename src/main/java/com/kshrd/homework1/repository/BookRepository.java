package com.kshrd.homework1.repository;

import com.kshrd.homework1.repository.dto.BookDto;
import com.kshrd.homework1.repository.dto.CategoriesDto;
import com.kshrd.homework1.repository.provider.BookProvider;
import com.kshrd.homework1.service.Util.Pagination;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

        @Select("SELECT COUNT(id) FROM tb_books")
        int countAllBooks();
        @Insert("INSERT INTO tb_books (title,author,description,thumbnail) VALUES (#{title},#{author},#{description},#{thumbnail})")
        boolean insert (BookDto bookDto);

        @Select("SELECT * FROM tb_books LIMIT #{pagination.limit}  OFFSET #{pagination.offset}")
//        @Results({
//                @Result(column = "cate_id", property = "cate_id", many = @Many(select = "selectCatById")),
//        })
        List<BookDto> select(@Param("pagination") Pagination pagination);
//        @SelectProvider(value = BookProvider.class, method = "selectCategoryTitleById")
        @Select("SELECT title FROM tb_categories WHERE id=#{id}")
        CategoriesDto selectCategoryTitleById(int cate_id);
//        @DeleteProvider(value = CategoryProvider.class,method = "Delete")
//        boolean delete(int id);
        @Delete("DELETE FROM tb_books WHERE id=#{id}")
        boolean delete(int id);
        @Select("SELECT * FROM tb_books WHERE id=#{id} ")
        BookDto findId(int id);
//        @Select("SELECT * FROM tb_books WHERE title LIKE '%'||#{filter.title}||''% ")
//        BookDto se
        @Update("UPDATE tb_books SET title=#{title},author=#{author},thumbnail=#{thumbnail},description=#{description}" +
                " WHERE id=#{id}")
        boolean update(BookDto bookDto);

}
